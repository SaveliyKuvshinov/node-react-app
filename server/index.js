const express = require("express");

const PORT = process.env.PORT || 3001;

const app = express();

let counter = 0;

app.get("/works", (req, res) => {
    counter++; 
    res.json({ message: `Worked ${counter} times!`});
})

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});